<?php

class Administrateur
{
    private ?int $id;
    private ?Client $client;
    private ?Agence $agence;
    private ?Compte $compte;

    public function __construct(
        ?Client $client = null,
        ?Agence $agence = null

    ) {
        $this->id = null;
        $this->client = $client;
        $this->agence = $agence;
    }
      /**
     * Get the value of id
     *
     * @return  mixed
     */
    public function getId(): int
    {
        return $this->id;
    }

      /**
     * Set the value of id
     *
     * @param   mixed  $id  
     *
     * @return  self
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

     /**
     * Get the value of client
     *
     * @return  mixed
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * Set the value of client
     *
     * @param   mixed  $client  
     *
     * @return  self
     */
    public function setClient(Client $client)
    {
        $this->client = $client;
    }

     /**
     * Get the value of agence
     *
     * @return  mixed
     */
    public function getAgence(): Agence
    {
        return $this->agence;
    }

    /**
     * Set the value of agence
     *
     * @param   mixed  $agence  
     *
     * @return  self
     */
    public function setAgence(Agence $agence)
    {
        $this->agence = $agence;
    }

  /*  public function toArray(): array
    {
        $tab=[];
        $tab[]=$this->id;
        $tab[]=$this->numeroClient;
        $tab[]=$this->nom;
        $tab[]=$this->prenom;
        $tab[]=$this->dateNaissance->format("Y-m-d");
        $tab[]=$this->telephone ;
        $tab[]=$this->email;
        $tab[]=$this->adresse; 
        return $tab;
    }*/


}

?>