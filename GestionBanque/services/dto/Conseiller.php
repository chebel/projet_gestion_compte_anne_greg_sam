<?php

class Conseiller
{
    public int $id;
    public Client $client;
    public string $domicile;

    public function __construct(
        ?string $client = null,
        ?string $domicile = null
    ) {
        $this->id = null;
        $this->client = $client;
        $this->domicile = $domicile;
    }

    public function getId(): int
    {
        return $this->id;
    }
    public function setId(int $id)
    {
        $this->id = $id;
    }
    public function getClient(): string
    {
        return $this->Client;
    }
    public function setClient(string $client)
    {
        $this->Client = $client;
    }
    public function getDomicile(): string
    {
        return $this->domicile;
    }
    public function setDomicile(string $domicile)
    {
        $this->domicile = $domicile;
    }

}
?>