<?php
require_once "services/dao/AgenceDAO.php";
require_once "services/dto/Agence.php";
class CreerAgenceController
{

    private AgenceDao $agenceDao;

    public function __construct()
    {
        $this->agenceDao = new AgenceDao();
    }

    public function execute()
    {
        echo ("\n");
        echo (" ------------------------ CREER UNE AGENCE ------------------------ \n");
        $newAgence = Agence::AgenceEnterKeybord();
        //creation ok
        $resultat = $this->checkAllData($newAgence);
        if ($resultat===true) {
            $newAgence = $this->agenceDao->save($newAgence);
            echo "Agence cree avec succes: Code agence ".$newAgence->getId()."\n";

        } else { //creation ko
            echo "Les Erreurs  : \n";
            foreach ($resultat as $erreurEnCours) {
                echo "  - $erreurEnCours\n";
            }
        }
    }

    private function checkAllData(Agence $agence)
    {
        $estValide = true;
        $messageErreurs = []; 
        if (!$this->validerChamp("#^[a-zA-Z]+$#",$agence->getNom())) {
            $estValide = false;
            $messageErreurs [] = "Le nom de l'agence est invalide !";
        }
        if (!$this->validerChamp("#^.+$#",$agence->getAdresse())) {
            $estValide = false;
            $messageErreurs [] = "L'adresse de l'agence est invalide !";
        }

        return $estValide?$estValide:$messageErreurs;
       
    }


    private function validerChamp(string $pattern, string $champ) : bool
    {
       return preg_match($pattern, $champ);
    }


}
