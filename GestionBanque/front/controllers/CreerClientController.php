<?php
require_once "services/dao/ClientDAO.php";
require_once "services/dto/Client.php";
class CreerClientController
{
    
    private function validerChamp(string $pattern, $champ): bool
    {      
        if ($champ === null)
                return false;
        if (!is_object($champ) && gettype($champ) === "string")
            return preg_match($pattern, $champ);
        return true;
    }
    private ClientDao $clientDao;

    public function __construct()
    {
        $this->clientDao = new ClientDao();
    }

    public function execute()
    {
        echo ("\n");
        echo (" ------------------------ CREER UN CLIENT ------------------------ \n");
        $newClient = Client::ClientEnterKeybord();
        //creation ok
        $resultat = $this->checkAllData($newClient);
        if ($resultat === true) {
            $newClient = $this->clientDao->save($newClient);
            echo "Client cree avec succes: Numero client ".$newClient->getNumeroClient()."\n";

        } else { //creation ko
            echo "Les Erreurs  : \n";
            foreach ($resultat as $erreurEnCours) {
                echo "  - $erreurEnCours\n";
            }
        }
    }

    private function checkAllData(Client $client)
    {
        $estValide = true;
        $messageErreurs = [];
        if (!$this->validerChamp("#^[a-zA-Z]+$#", $client->getNom())) {
            $estValide = false;
            $messageErreurs[] = "Le nom du client est invalide !";
        }
        if (!$this->validerChamp("#^[a-zA-Z]+$#", $client->getPrenom())) {
            $estValide = false;
            $messageErreurs[] = "Le prenom du client est invalide !";
        }
        if (!$this->validerChamp("#^.+$#", $client->getDateNaissance())) {
            $estValide = false;
            $messageErreurs[] = "La date de naissance du client est invalide !";
        }
        if (!$this->validerChamp("#^.+$#", $client->getTelephone())) {
            $estValide = false;
            $messageErreurs[] = "Le telephone du client est invalide !";
        }
        if (!$this->validerChamp("#^.+$#", $client->getEmail())) {
            $estValide = false;
            $messageErreurs[] = "L'email du client est invalide !";
        }

        return $estValide ? $estValide : $messageErreurs;
    }


}
