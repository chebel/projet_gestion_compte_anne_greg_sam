<?php
require_once "services/dao/AgenceDAO.php";
require_once "services/dao/ClientDAO.php";
require_once "services/dao/CompteDAO.php";
require_once "services/dto/Agence.php";
require_once "services/dto/Client.php";
require_once "services/dto/Compte.php";
class CreerCompteController
{

    private AgenceDao $agenceDao;
    private ClientDao $clientDao;
    private CompteDao $compteDao;

    public function __construct()
    {
        $this->agenceDao = new AgenceDao();
        $this->clientDao = new ClientDao();
        $this->compteDao = new CompteDao();
    }

    public function execute()
    {
        echo ("\n");
        echo (" ------------------------ CREER UN COMPTE ------------------------ \n");
        $newCompte = Compte::CompteEnterKeybord();
        //creation ok
        $resultat = $this->checkAllData($newCompte);
        if ($resultat === true) {
            $newCompte = $this->compteDao->save($newCompte);
            echo "Compte cree avec succes: Numero du compte " . $newCompte->getId() . "\n";
        } else { //creation ko
            echo "Les Erreurs  : \n";
            foreach ($resultat as $erreurEnCours) {
                echo "  - $erreurEnCours\n";
            }
        }
    }

    private function checkAllData(Compte $compte)
    {
        $estValide = true;
        $messageErreurs = [];
        $client=$this->clientDao->getById($compte->getClient()->getId());
        if ($client===null) {
            $estValide = false;
            $messageErreurs[] = "Le client dont l'id ".$compte->getClient()->getId()." n'existe pas !";
        }else{
            $compte->setClient($client);
        }
        $agence=$this->agenceDao->getById($compte->getAgence()->getId());
        if ($agence===null) {
            $estValide = false;
            $messageErreurs[] = "L'agence dont l'id ".$compte->getAgence()->getId()." n'existe pas !";
        }else{
            $compte->setAgence($agence);
        }
        if ($this->compteDao->possedUnCompteDeType($compte->getClient()->getId(), $compte->getType())) {
            $estValide = false;
            $messageErreurs[] = "L'utilisateur possede deja un compte de type ".$compte->getType()." !";
        }
        if ($compte->getSolde()<0 and !$compte->getEstAutorise()) {
            $estValide = false;
            $messageErreurs[] = "Solde negatif non autorise!";
        }

        return $estValide ? $estValide : $messageErreurs;
    }


    private function validerChamp(string $pattern, string $champ): bool
    {
        return preg_match($pattern, $champ);
    }
}
